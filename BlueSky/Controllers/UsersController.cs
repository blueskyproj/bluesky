﻿using BlueSky.Classes;
using BlueSky.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Diagnostics;

namespace BlueSky.Controllers
{
    public class UsersController : ApiController
    {
        // GET: api/Users
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Users/5
        public string Get(int id)
        {
            return "value";
        }
        [Route("api/Users/Login")]
        [HttpPost]
        public HttpResponseMessage Login(Login lg)

        {

            try
            {
                    var con = new SqlConnection(ConfigurationManager.ConnectionStrings["BlueSkyConnectionString"].ConnectionString);

                //var sql = @"select  count(*) FROM dbo.Users Where U_Email = '" + lg.email + "' and U_Password = '" + lg.password + "'";
                var sql = @"select U_ID , U_FirstName, U_LastName, U_Email, U_Picture, U_Status FROM dbo.Users Where U_Email = '" + lg.email + "' and U_Password = '" + lg.password + "'";
                

                SqlCommand comd = new SqlCommand(sql, con);
                con.Open();

                using (SqlDataReader response = comd.ExecuteReader())
                {
                    var dataList = response.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                    {

                        iU_ID = Convert.ToInt32(x["U_ID"]),
                        sU_FirstName = x["U_FirstName"].ToString(),
                        sU_LastName = x["U_LastName"].ToString(),
                        sU_Email = x["U_Email"].ToString(),
                        sU_Picture = x["U_Picture"].ToString(),
                        iU_Status = Convert.ToInt32(x["U_Status"]),
                    });

                    int iU_ID = 0;
                    string sU_FirstName = "";
                    string sU_LastName = "";
                    string sU_Email = "";
                    string sU_Picture = "";
                    int iU_Status = 0;

                    foreach (var row in dataList.AsEnumerable())  // AsEnumerable() returns IEnumerable<DataRow>
                    {
                        iU_ID = row.iU_ID;
                        sU_FirstName = row.sU_FirstName;
                        sU_LastName = row.sU_LastName;
                        sU_Email = row.sU_Email;
                        sU_Picture = row.sU_Picture;
                        iU_Status = row.iU_Status;
                    }

                    //return Request.CreateResponse(dataList);

                    if (iU_ID != 0)
                    {

                        var pet = new { Status = "Success", U_ID = iU_ID, U_FirstName  = sU_FirstName , U_LastName = sU_LastName , U_Email = sU_Email, U_Picture = sU_Picture , U_Status = iU_Status };
                        return Request.CreateResponse(HttpStatusCode.OK, pet);
                    }
                    else
                    {
                        var pet = new { Status = "Failed" };
                        return Request.CreateResponse(HttpStatusCode.OK, pet);
                    }
                }


                //int iU_ID = Convert.ToInt32(comd.ExecuteScalar());
                //string sU_FirstName = U_FirstName.ToString();
                //string sU_LastName = U_LastName;
                //string sU_Email = U_Email;
                //string sU_Picture = U_Picture;
                //string sU_Status = U_Status;

                //if (iU_ID > 0) {

                //    var pet = new { Status = "Success", U_ID = iU_ID };
                //    return Request.CreateResponse(HttpStatusCode.OK, pet);
                //}
                //else
                //{
                //    var pet = new { Status = "Failed" };
                //    return Request.CreateResponse(HttpStatusCode.OK, pet);
                //}

            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new { Status = "Failed" };
                return Request.CreateResponse(HttpStatusCode.OK, pet);
            }
        }

        // POST: api/Users
        public string Post(User usr)

        {

            try
            {
                string query = @"
                    exec Users_Insert 
                    '" + usr.U_FirstName + @"'
                    ,'" + usr.U_LastName + @"'
                    ,'" + usr.U_Title + @"'
                    ,'" + usr.U_Email + @"'
                    ,'" + usr.U_Phone + @"'
                    ,'" + usr.U_Username + @"'
                    ,'" + usr.U_Password + @"'
                    
                    ";

                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.
                    ConnectionStrings["BlueSkyConnectionString"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }

                return "User added Successfully!!";
            }
            catch (Exception)
            {

                return "Failed to Add!!";
            }
        }



        // POST: api/usersupdate
        [Route("api/usersupdate/")]
        [HttpPost]
        public string Postusersupdate(User usr)

        {

            try
            {
                string query = @"
                    exec Users_Update
                    
                    '" + usr.U_FirstName + @"'
                    ,'" + usr.U_LastName + @"'
                    ,'" + usr.U_Title + @"'
                    ,'" + usr.U_Email + @"'
                    ,'" + usr.U_Phone + @"'
                    ,'" + usr.U_Username + @"'
                    ,'" + usr.U_ID + @"'
                    
                    ";

                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.
                    ConnectionStrings["BlueSkyConnectionString"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }

                return "User updated Successfully!!";
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                return "Failed to Add!!";
            }
        }

        [Route("api/userprofile/{iU_ID}")]
        [HttpGet]
        public HttpResponseMessage userprofile(int iU_ID)
        {

            Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), "123");

            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("Users_Select", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@U_ID ", iU_ID);
                        using (SqlDataReader response = command.ExecuteReader())
                        {
                            var dataList = response.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {

                                U_ID = x["U_ID"].ToString(),
                                U_Username = x["U_Username"].ToString(),
                                U_FirstName = x["U_FirstName"].ToString(),
                                U_LastName = x["U_LastName"].ToString(),
                                U_Title = x["U_Title"].ToString(),
                                U_Gender = x["U_Gender"].ToString(),
                                U_Email = x["U_Email"].ToString(),
                                U_Phone = x["U_Phone"].ToString(),
                                Status = "Success"
                            });

                            return Request.CreateResponse(dataList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }



        // PUT: api/Users/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Users/5
        public void Delete(int id)
        {
        }
    }
}
