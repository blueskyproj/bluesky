﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BlueSky.Classes;
using BlueSky.Models;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;

namespace BlueSky.Controllers
{
    public class CountryController : ApiController
    {

        public object ErrStatus { get; private set; }
        public object ErrMessage { get; private set; }
        // GET api/Country
        public HttpResponseMessage Get()
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    DataTable table = new DataTable();

                    using (var command = new SqlCommand("Country_Select", connection))
                    {

                        command.CommandType = CommandType.StoredProcedure;
                        using (SqlDataReader country = command.ExecuteReader())
                        {
                            var dataList = country.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(p => new
                            {

                                C_ID = Convert.ToInt32(p["C_ID"]),
                                C_Code = p["C_Code"].ToString(),
                                C_Name = p["C_Name"].ToString(),
                                C_CodeName = p["C_CodeName"].ToString(),

                            });

                            return Request.CreateResponse(HttpStatusCode.OK, dataList);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }
    }
}