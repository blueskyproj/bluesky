﻿using BlueSky.Classes;
using BlueSky.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BlueSky.Controllers
{
    public class FundBrokerController : ApiController
    {
        public object ErrStatus { get; private set; }
        public object ErrMessage { get; private set; }
        // GET api/fundsummary
        public HttpResponseMessage Get()
        {
            try
            {
                return Request.CreateResponse("fundbroker");

            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }
        [Route("api/fundbroker/{iFM_ID}")]
        [HttpGet]
        public HttpResponseMessage fundbroker(int iFM_ID)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("FundBroker_Select", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Fund ", iFM_ID);
                        using (SqlDataReader response = command.ExecuteReader())
                        {
                            var dataList = response.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {
                                FB_ID = x["FB_ID"].ToString(),
                                FB_FM_ID = x["FB_FM_ID"].ToString(),
                                FB_B_ID = x["FB_B_ID"].ToString(),
                                B_BrokerName = x["B_BrokerName"].ToString(),
                                B_BrokerState = x["B_BrokerState"].ToString(),
                                B_BrokerCountry = x["B_BrokerCountry"].ToString(),
                                B_BrokerPhone = x["B_BrokerPhone"].ToString(),
                                Status = "Success"
                            });

                            var errormsg = new { Status = "Error", Message = "Not allowed" };
                            var pet = new { Data = ErrStatus = dataList, AdditionalParameters = ErrMessage = errormsg };
                            return Request.CreateResponse(pet);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }
        [Route("api/brokerfundbrokerreverse/{iFM_ID}")]
        [HttpGet]
        public HttpResponseMessage brokerfundbrokerreverse(int iFM_ID)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("[Broker_FundBroker_Select_Reverse]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Fund ", iFM_ID);
                        using (SqlDataReader response = command.ExecuteReader())
                        {
                            var dataList = response.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(p => new
                            {
                                B_ID = Convert.ToInt32(p["B_ID"]),
                                B_BrokerName = p["B_BrokerName"].ToString(),
                                B_BrokerStreet = p["B_BrokerStreet"].ToString(),
                                B_BrokerStreet2 = p["B_BrokerStreet2"].ToString(),
                                B_BrokerCity = p["B_BrokerCity"].ToString(),
                                B_BrokerState = p["B_BrokerState"].ToString(),
                                B_BrokerCountry = p["B_BrokerCountry"].ToString(),
                                B_BrokerZip = p["B_BrokerZip"].ToString(),
                                B_BrokerPhone = p["B_BrokerPhone"].ToString(),
                                B_BrokerCRD = p["B_BrokerCRD"].ToString()
                            });


                            return Request.CreateResponse(HttpStatusCode.OK, dataList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }
        public string Post(FundBroker fb)
        {

            //if (fm.Step == "1")
            //{
            try
            {
                string query = @"

                        exec [FundBroker_Insert]
                         " + fb.FB_FM_ID + @"
                        , " + fb.FB_B_ID + @"
                        ";

                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["BlueSkyConnectionString"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }

                return "Completed Successfully!!";
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                return "Failed to Complete!!";
            }
            //}
            //    else
            //    {
            //        return "Nothing to Do";
            //    }
        }
        [Route("api/fundbroker/{iFB_ID}")]
        [HttpDelete]
        // DELETE api/<controller>/5
        public void fundbrokerdelete(int iFB_ID)
        {
            //int UserID = 123;

            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("FundBroker_Delete", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@FB_ID", iFB_ID);
                        command.ExecuteNonQuery();
                    }
                }

                //return "success";
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                //return "failed";
            }
        }
    }


}