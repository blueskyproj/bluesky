﻿using BlueSky.Classes;
using BlueSky.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BlueSky.Controllers
{
    public class AddressController : ApiController
    {
        // GET: Address
        [Route("api/address/{iFM_ID}")]
        [HttpGet]
        public HttpResponseMessage Address(int iFM_ID)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("Address_Get_Fund_Default_Address", connection))
                    {

                        int iType = 1;

                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Fund", iFM_ID);
                        command.Parameters.AddWithValue("@Type", iType);
                        using (SqlDataReader response = command.ExecuteReader())
                        {
                            var dataList = response.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {

                                Adr_ID = x["Adr_ID"].ToString(),
                                Adr_Type = x["Adr_Type"].ToString(),
                                Adr_Link_ID = x["Adr_Link_ID"].ToString(),
                                Adr_Line1 = x["Adr_Line1"].ToString(),
                                Adr_Line2 = x["Adr_Line2"].ToString(),
                                Adr_City = x["Adr_City"].ToString(),
                                Adr_State = x["Adr_State"].ToString(),
                                Adr_Zip = x["Adr_Zip"].ToString(),
                                Adr_Country = x["Adr_Country"].ToString(),
                                Adr_Phone = x["Adr_Phone"].ToString(),
                                Adr_Default = x["Adr_Default"].ToString(),

                                Status = "Success"
                            });

                            return Request.CreateResponse(dataList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }

        // POST api/<controller>

        public string Post(Address ad)
        {
            try
            {

                string query = @"


                    exec [Address_InsertUpdate]

                        '" + ad.UserID + @"'
                    , '" + ad.Adr_ID + @"'
                    , '" + ad.Adr_Type + @"'
                    ,'" + ad.Adr_Link_ID + @"'
                    ,'" + ad.Adr_Line1 + @"'
                    ,'" + ad.Adr_Line2 + @"'
                    ,'" + ad.Adr_City + @"'
                    ,'" + ad.Adr_State + @"'
                    ,'" + ad.Adr_Zip + @"'
                    ,'" + ad.Adr_Country + @"'
                    ,'" + ad.Adr_Phone + @"'
                    ,'" + ad.Adr_Default + @"'
                    ";

                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["BlueSkyConnectionString"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }

                return "Completed Successfully!!";
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                return "Failed to Complete!!";
            }
        }
    }
}