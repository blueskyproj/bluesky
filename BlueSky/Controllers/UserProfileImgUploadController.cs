﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using BlueSky.Classes;
using BlueSky.Models;
using System.Net;
using System.Drawing;
using System.Configuration;

namespace BlueSky.Controllers
{
    public class UserProfileImgUploadController : Controller
    {
        // GET: UserProfileImgUpload
        public ActionResult Index()
        {
            return View();
        }

        [Route("UserProfileImgUpload/FileUploads/")]
        [HttpPost]
        public string FileUploads()
        {
   
            if (Request.Form["file"] != null)
               //if (Request.Files.Count > 0)
                {
                try
                {
                    string sFileLocation = ConfigurationManager.AppSettings["FileLocation"];
                    //  Get all files from Request object  
                    string base64String = Request.Form["file"];
                    //string userid = "3";
                    string userid = Request.Form["userid"];


                    base64String = base64String.Replace("file: data: image / png; base64,", "");
                    base64String = base64String.Replace("data: image / png; base64,", "");
                    base64String = base64String.Replace("file:data:image/png;base64,", "");
                    base64String = base64String.Replace("data:image/png;base64,", "");

                    string filePath = sFileLocation + "avatars\\avatar_" + userid + ".png";

                    byte[] bytes = Convert.FromBase64String(base64String);

                    Image image;
                    using (MemoryStream ms = new MemoryStream(bytes))
                    {
                        image = Image.FromStream(ms);
                    }

                    image.Save(filePath, System.Drawing.Imaging.ImageFormat.Png);


                    return "User updated Successfully!!";
                }
                catch (Exception ex)
                {
                    Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                    return ex.Message;
                }
            }
            else
            {
                return "Error: No files selected.";
            }
        }
    }
}