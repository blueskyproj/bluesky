﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BlueSky.Classes;
using BlueSky.Models;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;

namespace BlueSky.Controllers
{
    public class BrokersController : ApiController
    {

        public object ErrStatus { get; private set; }
        public object ErrMessage { get; private set; }
        // GET api/brokers
        public HttpResponseMessage Get()
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    DataTable table = new DataTable();

                    using (var command = new SqlCommand("Brokers_Select", connection))
                    {

                        command.CommandType = CommandType.StoredProcedure;
                        using (SqlDataReader country = command.ExecuteReader())
                        {
                            var dataList = country.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(p => new
                            {
                                B_ID = Convert.ToInt32(p["B_ID"]),
                                B_BrokerName = p["B_BrokerName"].ToString(),
                                B_BrokerStreet = p["B_BrokerStreet"].ToString(),
                                B_BrokerStreet2 = p["B_BrokerStreet2"].ToString(),
                                B_BrokerCity = p["B_BrokerCity"].ToString(),
                                B_BrokerState = p["B_BrokerState"].ToString(),
                                B_BrokerCountry = p["B_BrokerCountry"].ToString(),
                                B_BrokerZip = p["B_BrokerZip"].ToString(),
                                B_BrokerPhone = p["B_BrokerPhone"].ToString(),
                                B_BrokerCRD = p["B_BrokerCRD"].ToString()
                            });

                            return Request.CreateResponse(HttpStatusCode.OK, dataList);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }
    }
}