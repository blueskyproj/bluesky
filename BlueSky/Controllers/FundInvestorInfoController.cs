﻿using BlueSky.Classes;
using BlueSky.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BlueSky.Controllers
{
    public class FundInvestorInfoController : ApiController
    {
        public object ErrStatus { get; private set; }
        public object ErrMessage { get; private set; }
        // GET api/fundsummary
        public HttpResponseMessage Get()
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("FS_Fund_Summary", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@user", 1);
                        using (SqlDataReader contracts = command.ExecuteReader())
                        {
                            var dataList = contracts.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(p => new
                            {
                                FM_ID = Convert.ToInt32(p["FM_ID"]),
                                FM_Name = p["FM_Name"].ToString(),
                                Investors = p["Investors"].ToString(),
                                Amount = p["Amount"].ToString(),
                                Status = "Error"
                            });
                            var errormsg = new { Status = "Error", Message = "Not allowed" };
                            var pet = new { Data = ErrStatus = dataList, AdditionalParameters = ErrMessage = errormsg };
                            return Request.CreateResponse(pet);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }

        // POST api/<controller>

        public string Post(FundInvestorInfoCompositeObject fmi)
        {

            //if (fm.Step == "1")
            //{
            try
            {
                string query = @"

                        exec [FundInvestorInfo_InsertUpdate]
                         " + fmi.FundInvestorInfo.FI_ID + @"
                        , " + fmi.FundInvestorInfo.FI_FM_ID + @"
                        , '" + fmi.FundInvestorInfo.FI_Name + @"'
                        , " + fmi.FundInvestorInfo.FI_Country + @"
                        , " + fmi.FundInvestorInfo.FI_Jur + @"
                        , " + fmi.FundInvestorInfo.FI_SubsAmnt + @"
                        , '" + fmi.FundInvestorInfo.FI_CommitDate + @"'
                        , " + fmi.FundInvestorInfo.FI_New + @"
                        , " + fmi.User.U_ID + @"
                        ";

                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["BlueSkyConnectionString"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }

                return "Completed Successfully!!";
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                return "Failed to Complete!!";
            }
        //}
        //    else
        //    {
        //        return "Nothing to Do";
        //    }
        }

        [Route("api/FundInvestorInfo/{iFM_ID}")]
        [HttpGet]
        public HttpResponseMessage Get(int iFM_ID)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("FundInvestorInfo_Select", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@iFM_ID", iFM_ID);
                        using (SqlDataReader contracts = command.ExecuteReader())
                        {
                            var dataList = contracts.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(p => new
                            {
                                FI_ID = Convert.ToInt32(p["FI_ID"]),
                                FI_Name = p["FI_Name"].ToString(),
                                FI_Country = p["FI_Country"].ToString(),
                                FI_Jur = p["FI_Jur"].ToString(),
                                FI_SubsAmnt = p["FI_SubsAmnt"].ToString(),
                                FI_CommitDate = p["FI_CommitDate"].ToString(),
                                FI_New = p["FI_New"].ToString(),
                                FM_Currency = p["FM_Currency"].ToString(),
                                C_Name = p["C_Name"].ToString(),
                                S_Name = p["S_Name"].ToString(),
                                Status = "Success"
                            });
                            var errormsg = new { Status = "Error", Message = "Not allowed" };
                            var pet = new { Data = ErrStatus = dataList, AdditionalParameters = ErrMessage = errormsg };
                            return Request.CreateResponse(pet);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }
        }

        [Route("api/FundInvestorInfoDetails/{iFI_ID}")]
        [HttpGet]
        public HttpResponseMessage FundInvestorInfoDetails(int iFI_ID)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("FundInvestorInfo_Select_FIID", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@iFI_ID", iFI_ID);
                        using (SqlDataReader contracts = command.ExecuteReader())
                        {
                            var dataList = contracts.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(p => new
                            {
                                FI_ID = Convert.ToInt32(p["FI_ID"]),
                                FI_FM_ID = Convert.ToInt32(p["FI_FM_ID"]),
                                FI_Name = p["FI_Name"].ToString(),
                                FI_Country = Convert.ToInt32(p["FI_Country"]),
                                FI_Jur = Convert.ToInt32(p["FI_Jur"]),
                                FI_SubsAmnt = p["FI_SubsAmnt"].ToString(),
                                FI_CommitDate = p["FI_CommitDate"].ToString(),
                                FI_New = Convert.ToInt32(p["FI_New"]), 
                                Status = "Success"
                            });
                            //var errormsg = new { Status = "Error", Message = "Not allowed" };
                            //var pet = new { Data = ErrStatus = dataList, AdditionalParameters = ErrMessage = errormsg };
                            //return Request.CreateResponse(pet);
                            return Request.CreateResponse(dataList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody] string value)
        {
        }

        [Route("api/FundInvestorInfo/{iFM_ID}")]
        [HttpDelete]
        // DELETE api/<controller>/5
        public HttpResponseMessage Delete(int iFM_ID)
        {
            try
            {
                string query = @"

                        exec [FundInvestorInfo_Delete]
                         " + iFM_ID + @"
                        ";

                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["BlueSkyConnectionString"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }

                var pet = new { Status = "Success", Message = "Record Deleted" };
                return Request.CreateResponse(HttpStatusCode.OK, pet);
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }
        }
    }
}