﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BlueSky.Classes;
using BlueSky.Models;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;

namespace BlueSky.Controllers
{
    public class EntityTypeController : ApiController
    {

        public object ErrStatus { get; private set; }
        public object ErrMessage { get; private set; }
        // GET api/Country
        public HttpResponseMessage Get()
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    DataTable table = new DataTable();

                    using (var command = new SqlCommand("EntityType_Select", connection))
                    {

                        command.CommandType = CommandType.StoredProcedure;
                        using (SqlDataReader entitytype = command.ExecuteReader())
                        {
                            var dataList = entitytype.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(p => new
                            {
                                ET_ID = Convert.ToInt32(p["ET_ID"]),
                                ET_Desc = p["ET_Desc"].ToString(),
                            });

                            return Request.CreateResponse(HttpStatusCode.OK, dataList);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }
    }
}
