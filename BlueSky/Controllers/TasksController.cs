﻿using BlueSky.Classes;
using BlueSky.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace BlueSky.Controllers
{
    public class TasksController : ApiController
    {
        public object ErrStatus { get; private set; }
        public object ErrMessage { get; private set; }
        // GET api/tasks
        public HttpResponseMessage Get()
        {
            try
            {
                return Request.CreateResponse("123");

            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }

        // POST api/<controller>

        [Route("api/Tasks/{U_ID}")]
        [HttpGet]
        public HttpResponseMessage Get(int U_ID)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("Tasks_Select", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@U_ID", U_ID);
                        using (SqlDataReader contracts = command.ExecuteReader())
                        {
                            var tasksList = contracts.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(p => new
                            {
                                Tsk_ID = Convert.ToInt32(p["Tsk_ID"]),
                                Tsk_Section = p["Tsk_Section"].ToString(),
                                TS_Section = p["TS_Section"].ToString(),
                                Tsk_Title = p["Tsk_Title"].ToString(),
                                Tsk_Text = p["Tsk_Text"].ToString(),
                                Tsk_Priority = p["Tsk_Priority"].ToString(),
                                Tsk_Status = p["Tsk_Status"].ToString(),
                                Tsk_FN_ID = p["Tsk_FN_ID"].ToString(),
                                //Tsk_Deadline = "2021-05-05",
                                Tsk_Deadline = p["Tsk_Deadline"].ToString(),
                                Tsk_Date_Done = p["Tsk_Date_Done"].ToString(),
                                Tsk_U_ID_allocated = p["Tsk_U_ID_allocated"].ToString(),
                                Tsk_U_ID_done = p["Tsk_U_ID_done"].ToString(),

                                Status = "Success"
                            });
                              return Request.CreateResponse(HttpStatusCode.OK, tasksList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }
        }


        [Route("api/TaskSections/{U_ID}")]
        [HttpGet]
        public HttpResponseMessage GetTaskSections(int U_ID = 0)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("Task_Sections_Select", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@U_ID", U_ID);
                        using (SqlDataReader contracts = command.ExecuteReader())
                        {
                            var tasksList = contracts.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(p => new
                            {
                                TS_ID = Convert.ToInt32(p["TS_ID"]),
                                TS_Section = p["TS_Section"].ToString(),
                                Status = "Success"
                            });
                            var errormsg = new { Status = "Error", Message = "Not allowed" };
                            var pet = new { Data = ErrStatus = tasksList, AdditionalParameters = ErrMessage = errormsg };
                            //var pet2 = new { Data = tasksList};
                            return Request.CreateResponse(pet);
                            //return Request.CreateResponse(pet2);

                            //return Request.CreateResponse(HttpStatusCode.OK, tasksList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }
        }

        [Route("api/GetAllSections/{U_ID}")]
        [HttpGet]
        public HttpResponseMessage GetAllSections(int U_ID = 0)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("Task_Sections_All", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@U_ID", U_ID);
                        using (SqlDataReader contracts = command.ExecuteReader())
                        {
                            var tasksList = contracts.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(p => new
                            {
                                TS_ID = Convert.ToInt32(p["TS_ID"]),
                                TS_Section = p["TS_Section"].ToString(),
                                Status = "Success"
                            });
                            var errormsg = new { Status = "Error", Message = "Not allowed" };
                            var pet = new { Data = ErrStatus = tasksList, AdditionalParameters = ErrMessage = errormsg };
                            //var pet2 = new { Data = tasksList};
                            return Request.CreateResponse(pet);
                            //return Request.CreateResponse(pet2);

                            //return Request.CreateResponse(HttpStatusCode.OK, tasksList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }
        }

        [Route("api/Tasks/GetUsersToAssign/")]
        [HttpGet]
        public HttpResponseMessage GetUsersToAssign()
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("Users_Retrieve", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        using (SqlDataReader users = command.ExecuteReader())
                        {
                            var usersList = users.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(p => new
                            {
                                UserID = Convert.ToInt32(p["U_ID"]),
                                UserName = p["U_Username"].ToString(),
                                LastName = p["U_LastName"].ToString(),
                                FirstName = p["U_FirstName"].ToString()
                            });

                            return Request.CreateResponse(usersList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.OK, pet);
            }
        }


        [Route("api/tasksections2/addupdatetask")]
        [HttpPost]
        public string PostTask(FullTasklist t)

        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("Tasks_InsertUpdate", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Tsk_ID", t.id);
                        command.Parameters.AddWithValue("@Tsk_Section", t.section);
                        command.Parameters.AddWithValue("@Tsk_Title", t.title);
                        command.Parameters.AddWithValue("@Tsk_Text", t.notes);
                        command.Parameters.AddWithValue("@Tsk_Priority", t.priority);
                        command.Parameters.AddWithValue("@Tsk_FN_ID", t.fund);
                        command.Parameters.AddWithValue("@Tsk_U_ID_allocated", t.allocated);
                        command.Parameters.AddWithValue("@Tsk_U_ID_done", t.DoneBy);
                        command.Parameters.AddWithValue("@Tsk_Status", t.completed);
                        DateTime d2 = DateTime.Parse(t.dueDate, null, System.Globalization.DateTimeStyles.RoundtripKind).ToLocalTime();
                        command.Parameters.AddWithValue("@Tsk_Deadline", d2);
                        command.ExecuteNonQuery();
                    }
                }

                return "success";
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                return ex.Message;
            }
        }


        [Route("api/TaskSections/addsection")]
        [HttpPost]
        public string AddSection(Section sec)

        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("Task_Sections_Insert", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Section", sec.TS_Section);
                        command.ExecuteNonQuery();
                    }

                }

                return "success";
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                return "Failed";
            }
        }



        // PUT api/<controller>/5
        public void Put(int id, [FromBody] string value)
        {
        }

        [Route("api/Tasks/{iFM_ID}")]
        [HttpDelete]
        // DELETE api/<controller>/5
        public HttpResponseMessage Delete(int iFN_ID)
        {
            try
            {
                string query = @"

                        exec [FundNotice_Delete]
                         " + iFN_ID + @"
                        ";

                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["BlueSkyConnectionString"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }

                var pet = new { Status = "Success", Message = "Record Deleted" };
                return Request.CreateResponse(HttpStatusCode.OK, pet);
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }
        }
    }
}