﻿using BlueSky.Classes;
using BlueSky.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;

namespace BlueSky.Controllers
{
    public class IndustryGroupController : ApiController
    {

        public object ErrStatus { get; private set; }
        public object ErrMessage { get; private set; }
        // GET api/industrygroup
        public HttpResponseMessage Get()
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    DataTable table = new DataTable();

                    using (var command = new SqlCommand("IndustryGroup_Select", connection))
                    {

                        command.CommandType = CommandType.StoredProcedure;
                        using (SqlDataReader industryGroup = command.ExecuteReader())
                        {
                            var dataList = industryGroup.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(p => new
                            {
                                IG_ID = Convert.ToInt32(p["IG_ID"]),
                                IG_Group = p["IG_Group"].ToString()
                            });

                            return Request.CreateResponse(HttpStatusCode.OK, dataList);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }  
    }
}