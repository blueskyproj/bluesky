﻿using BlueSky.Classes;
using BlueSky.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.IO;
using System.Web;
//using System.Web.Mvc;


namespace BlueSky.Controllers
{
    public class DashboardController : ApiController
    {
        public object ErrStatus { get; private set; }
        public object ErrMessage { get; private set; }

        // GET api/Dashboard
        [Route("api/dashboardfs")]
        [HttpGet]
        public HttpResponseMessage Get()
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("FundMaster_Select_Top5", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        using (SqlDataReader contracts = command.ExecuteReader())
                        {
                            var top5List = contracts.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(p => new
                            {
                                FM_ID = Convert.ToInt32(p["FM_ID"]),
                                FM_Name = p["FM_Name"].ToString(),
                                FM_DateFirstSale = p["FM_DateFirstSale"].ToString(),
                                Status = "Success"
                            });
                            var errormsg = new { Status = "Error", Message = "Not allowed" };
                            var pet = new { Data = ErrStatus = top5List, AdditionalParameters = ErrMessage = errormsg };
                            return Request.CreateResponse(pet);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }
           
        }

        // GET api/Dashboard
        [Route("api/dashboardtu5")]
        [HttpGet]
        public HttpResponseMessage GetTop5Upcoming()
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("Tasks_Top5_Upcoming", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        using (SqlDataReader contracts = command.ExecuteReader())
                        {
                            var top5List = contracts.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(p => new
                            {
                                Tsk_ID          = Convert.ToInt32(p["Tsk_ID"]),
                                FM_ID           = Convert.ToInt32(p["FM_ID"]),
                                FM_Name         = p["FM_Name"].ToString(),
                                Tsk_Deadline    = p["Tsk_Deadline"].ToString(),
                                S_Name          = p["S_Name"].ToString(),
                            });
                            var errormsg = new { Status = "Error", Message = "Not allowed" };
                            var pet = new { Data = ErrStatus = top5List, AdditionalParameters = ErrMessage = errormsg };
                            return Request.CreateResponse(pet);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }


        // GET api/Dashboard
        [Route("api/dashboardtd5")]
        [HttpGet]
        public HttpResponseMessage GetTop5Done()
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("Tasks_Top5_Done", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        using (SqlDataReader contracts = command.ExecuteReader())
                        {
                            var top5List = contracts.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(p => new
                            {
                                Tsk_ID = Convert.ToInt32(p["Tsk_ID"]),
                                FM_ID = Convert.ToInt32(p["FM_ID"]),
                                FM_Name = p["FM_Name"].ToString(),
                                Tsk_Deadline = p["Tsk_Deadline"].ToString(),
                                S_Name = p["S_Name"].ToString(),
                            });
                            var errormsg = new { Status = "Error", Message = "Not allowed" };
                            var pet = new { Data = ErrStatus = top5List, AdditionalParameters = ErrMessage = errormsg };
                            return Request.CreateResponse(pet);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }

        // GET api/Dashboard
        [Route("api/dashboardte5")]
        [HttpGet]
        public HttpResponseMessage GetTop5Expired()
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("Tasks_Top5_Expired", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        using (SqlDataReader contracts = command.ExecuteReader())
                        {
                            var top5List = contracts.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(p => new
                            {
                                Tsk_ID = Convert.ToInt32(p["Tsk_ID"]),
                                FM_ID = Convert.ToInt32(p["FM_ID"]),
                                FM_Name = p["FM_Name"].ToString(),
                                Tsk_Deadline = p["Tsk_Deadline"].ToString(),
                                S_Name = p["S_Name"].ToString(),
                            });
                            var errormsg = new { Status = "Error", Message = "Not allowed" };
                            var pet = new { Data = ErrStatus = top5List, AdditionalParameters = ErrMessage = errormsg };
                            return Request.CreateResponse(pet);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }


    }
}