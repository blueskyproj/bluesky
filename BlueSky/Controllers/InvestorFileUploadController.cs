﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using BlueSky.Classes;
using BlueSky.Models;
using System.Net;
using System.Configuration;

namespace BlueSky.Controllers
{
    public class InvestorFileUploadController : Controller
    {
        // GET: InvestorFileUpload
        public ActionResult Index()
        {
            return View();
        }

        [Route("/InvestorFileUpload/FileUploads/")]
        [HttpPost]


        public string FileUploads()
        {
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    string userid = Request.Form["userid"];
                    string description = Request.Form["name"];
                    string fm_id = Request.Form["fm_id"];
                    int lastrecord = 0;
                    string fnam = "";

                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }
                       /* var supportedTypes = new[] { "csv" };
                        var fileExt = System.IO.Path.GetExtension(fname).Substring(1);
                        if (!supportedTypes.Contains(fileExt))
                        {
                            string ErrorMessage = "Error: File Extension Is InValid - Only CSV Files Allowed";
                            return ErrorMessage;
                        }*/


                        DateTime now = DateTime.Now;
                        //fnam = now.ToFileTime() + fname;
                        fnam = fname;
                        string sFileLocation = ConfigurationManager.AppSettings["FileLocation"];
                        fname = sFileLocation + "uploads\\" + fnam;

                        file.SaveAs(fname);
                        // Get the complete folder path and store the file inside it.  
                        using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                        {

                            connection.Open();
                             using (var command = new SqlCommand("TmpUpload1_Delete", connection))
                            {
                                command.CommandType = CommandType.StoredProcedure;
                                command.Parameters.AddWithValue("@User_ID", userid);
                                command.ExecuteNonQuery();
                            }

                            using (var command2 = new SqlCommand("SP_FileImporter_CreateRecord", connection))
                            {
                                command2.CommandType = CommandType.StoredProcedure;
                                command2.Parameters.AddWithValue("@FM_ID", fm_id);
                                command2.Parameters.AddWithValue("@FileCategoryID", 1);
                                command2.Parameters.AddWithValue("@FileName", fnam);
                                command2.Parameters.AddWithValue("@Description", description);
                                command2.Parameters.AddWithValue("@UploadedBy", userid);
                                lastrecord = (Int32)command2.ExecuteScalar();
                            }

                            using (var command3 = new SqlCommand("sp_FileImport", connection))
                            {
                                command3.CommandType = CommandType.StoredProcedure;
                                command3.Parameters.AddWithValue("@FileImporterUploadsID", lastrecord);
                                command3.ExecuteNonQuery();
                            }


                            using (var command4 = new SqlCommand("sp_FileImport_Validation_FundInvestor", connection))
                            {
                                command4.CommandType = CommandType.StoredProcedure;
                                command4.Parameters.AddWithValue("@UserID", userid);
                                command4.Parameters.AddWithValue("@FileImporterUploadsID", lastrecord);
                                command4.ExecuteNonQuery();
                            }

                            using (var command5 = new SqlCommand("sp_FileImport_Process_FundInvestor", connection))
                            {
                                command5.CommandType = CommandType.StoredProcedure;
                                command5.Parameters.AddWithValue("@UserID", userid);
                                command5.Parameters.AddWithValue("@FileImporterUploadsID", lastrecord);
                                command5.ExecuteNonQuery();
                            }

                            
                            connection.Close();
                        }
                      
                    }

                    return "File Uploaded Successfully";
                    //return "{ \"Status\" =\"Success\"}";

                }
                catch (Exception ex)
                {
                    Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                    return ex.Message.ToString();
                    //return "{ \"Status\" =\" " + ex.Message + "\"}";

                }
            }
            else
            {
                var ex = "Error: No files selected.";
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                return ex;
                //return "{ \"Status\" =\"Error: No files selected\"}";
            }
        }
    }
}