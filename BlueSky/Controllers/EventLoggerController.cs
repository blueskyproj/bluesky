﻿using BlueSky.Classes;
using BlueSky.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BlueSky.Controllers
{

public class EventLoggerController : ApiController
{
    public object ErrStatus { get; private set; }
    public object ErrMessage { get; private set; }
    // GET api/fundsummary
    public HttpResponseMessage Get()
    {
        try
        {
            using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
            {
                connection.Open();

                using (var command = new SqlCommand("Event_Logger_Last5", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@user", 1);
                    using (SqlDataReader contracts = command.ExecuteReader())
                    {
                        var contractList = contracts.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(p => new
                        {
                            EL_ID = Convert.ToInt32(p["EL_ID"]),
                            EL_Description = p["EL_Description"].ToString(),
                            EL_Date = p["EL_Date"].ToString(),
                            EL_User = Convert.ToInt32(p["EL_User"]),
                            EL_Fund = Convert.ToInt32(p["EL_Fund"]),
                            EL_TimeDiff = p["EL_TimeDiff"].ToString(),
                            Status = "Error"
                        });
                        var errormsg = new { Status = "Error", Message = "Not allowed" };
                        var pet = new { Data = ErrStatus = contractList, AdditionalParameters = ErrMessage = errormsg };
                        return Request.CreateResponse(pet);
                    }
                }

            }
        }
        catch (Exception ex)
        {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new { Status = "Error", Message = ex };
            return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
        }

    }

    // PUT api/<controller>/5
    public void Put(int id, [FromBody] string value)
    {
    }

    // DELETE api/<controller>/5
    public void Delete(int id)
    {
    }
}
}