﻿using BlueSky.Classes;
using BlueSky.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BlueSky.Controllers
{
    public class FundMasterController : ApiController
    {
        public object ErrStatus { get; private set; }
        public object ErrMessage { get; private set; }
        // GET api/fundsummary
        public HttpResponseMessage Get()
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("FS_Fund_Summary", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@user", 1);
                        using (SqlDataReader contracts = command.ExecuteReader())
                        {
                            var dataList = contracts.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(p => new
                            {
                                FM_ID = Convert.ToInt32(p["FM_ID"]),
                                FM_Name = p["FM_Name"].ToString(),
                                Investors = p["Investors"].ToString(),
                                Amount = p["Amount"].ToString(),
                                Status = "Error"
                            });
                            var errormsg = new {Status = "Error", Message = "Not allowed" };
                            var pet = new { Data = ErrStatus = dataList, AdditionalParameters = ErrMessage = errormsg};
                            return Request.CreateResponse(pet);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }
           
        }

        // POST api/<controller>

        public string Post(FundMaster fm)
        {

            if (fm.Step == "1")
            {
                try
                {
                    int lastrecord = 0;
                    var connection = new SqlConnection(ConnectionStrings.DBConnectionString);
                    connection.Open();
                    using (var command2 = new SqlCommand("FundMaster_Step1_InsertUpdate", connection))
                    {
                        command2.CommandType = CommandType.StoredProcedure;
                        command2.Parameters.AddWithValue("@UserID", fm.UserID);
                        command2.Parameters.AddWithValue("@FM_ID", fm.FM_ID);
                        command2.Parameters.AddWithValue("@FM_Name", fm.FM_Name);
                        command2.Parameters.AddWithValue("@FM_PreviousName", fm.FM_PreviousName);
                        command2.Parameters.AddWithValue("@FM_Industry_Group", fm.FM_Industry_Group);
                        command2.Parameters.AddWithValue("@FM_Federal_Exemption", fm.FM_Federal_Exemption);
                        lastrecord = (Int32)command2.ExecuteScalar();
                    }

                    return lastrecord.ToString();
                }
                catch (Exception ex)
                {

                    Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                    return "0";
                }
            }
            else if (fm.Step == "3")
            {
                try
                {

                    DateTime d1 = DateTime.Parse(fm.FM_DateFirstSale, null, System.Globalization.DateTimeStyles.RoundtripKind).ToLocalTime();
                    
                    string query = @"

                        exec [FundMaster_Step3_Update]
                         " + fm.UserID + @"
                         ," + fm.FM_ID + @"
                         ,'" + fm.FM_JurisOrg + @"'
                         ,'" + fm.FM_DateOrg + @"'
                         ,'" + fm.FM_EntityType + @"'
                         ,'" + d1.ToShortDateString() + @"'
                         ,'" + fm.FM_Currency + @"'
                         ,'" + fm.FM_MinInvest  + @"'
                        ";

                    DataTable table = new DataTable();
                    using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["BlueSkyConnectionString"].ConnectionString))
                    using (var cmd = new SqlCommand(query, con))
                    using (var da = new SqlDataAdapter(cmd))
                    {
                        cmd.CommandType = CommandType.Text;
                        da.Fill(table);
                    }

                    return "Completed Successfully!!";
                }
                catch (Exception ex)
                {
                    Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                    return "Failed to Complete!!";
                }
            }
            else if (fm.Step == "4")
            {
                try
                {
                    string query = @"

                        exec [FundMaster_Step4_Update]
                         " + fm.UserID + @"
                         ," + fm.FM_ID + @"
                         ,'" + fm.FM_MatterCode + @"'
                         ,'" + fm.FM_Client + @"'
                         ,'" + fm.FM_BillingPartner + @"'
                        , '" + fm.FM_PartnerResp + @"'
                        , '" + fm.FM_Team_Members + @"'
                        , '" + fm.FM_CIK + @"'
                        , '" + fm.FM_CCC + @"'
                        , '" + fm.FM_Passphrase + @"'
                        , '" + fm.FM_FormDFiling + @"'
                        ";

                    DataTable table = new DataTable();
                    using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["BlueSkyConnectionString"].ConnectionString))
                    using (var cmd = new SqlCommand(query, con))
                    using (var da = new SqlDataAdapter(cmd))
                    {
                        cmd.CommandType = CommandType.Text;
                        da.Fill(table);
                    }

                    return "Completed Successfully!!";
                }
                catch (Exception ex)
                {
                    Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                    return "Failed to Complete!!";
                }
            }
            else
            {
                return "Nothing to Do";
            }

        }

        [Route("api/fundmaster/{iFM_ID}")]
        [HttpGet]
        public HttpResponseMessage fundmaster(int iFM_ID)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("FundMaster_Select", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@iFM_ID ", iFM_ID);
                        using (SqlDataReader response = command.ExecuteReader())
                        {
                            var dataList = response.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {
                                
                                FM_ID = x["FM_ID"].ToString(),
                                FM_Name = x["FM_Name"].ToString(),
                                FM_PreviousName = x["FM_PreviousName"].ToString(),                                
                                FM_Industry_Group = x["FM_Industry_Group"].ToString(),
                                FM_Federal_Exemption = x["FM_Federal_Exemption"].ToString(),

                                FM_JurisOrg = x["FM_JurisOrg"].ToString(),
                                FM_DateOrg = x["FM_DateOrg"].ToString(),
                                FM_EntityType = x["FM_EntityType"].ToString(),
                                FM_Currency = x["FM_Currency"].ToString(),
                                FM_DateFirstSale = x["FM_DateFirstSale"].ToString(),
                                FM_1940ActExclusion = x["FM_1940ActExclusion"].ToString(),
                                FM_MinInvest = x["FM_MinInvest"].ToString(),

                                FM_MatterCode = x["FM_MatterCode"].ToString(),
                                FM_Client = x["FM_Client"].ToString(),
                                FM_BillingPartner = x["FM_BillingPartner"].ToString(),
                                FM_PartnerResp = x["FM_PartnerResp"].ToString(),
                                FM_Team_Members = x["FM_Team_Members"].ToString(),
                                FM_CIK = x["FM_CIK"].ToString(),
                                FM_CCC = x["FM_CCC"].ToString(),
                                FM_Passphrase = x["FM_Passphrase"].ToString(),
                                FM_FormDFiling = x["FM_FormDFiling"].ToString(),

                                Status = "Success"
                            });

                            var pet = new { Data = dataList, AdditionalParameters = "" };
                            return Request.CreateResponse(pet);
                            //return Request.CreateResponse(dataList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new {Data = "",  AdditionalParameters = ErrMessage = ex };
                return Request.CreateResponse(pet);
                //var pet = new { Status = "Error", Message = ex };
                //return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}