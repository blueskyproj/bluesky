﻿using BlueSky.Classes;
using BlueSky.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.IO;
using System.Web;
//using System.Web.Mvc;


namespace BlueSky.Controllers
{
    public class FundSummaryController : ApiController
    {
        public object ErrStatus { get; private set; }
        public object ErrMessage { get; private set; }
        // GET api/FundMaster
        [Route("api/fundsummary/{ibroker}/{sCIK}")]
        [HttpGet]
        public HttpResponseMessage Get(int iBroker, string sCIK)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("FS_Fund_Summary", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@user", 1);
                        command.Parameters.AddWithValue("@broker", iBroker);
                        command.Parameters.AddWithValue("@cik", sCIK);
                        using (SqlDataReader contracts = command.ExecuteReader())
                        {
                            var contractList = contracts.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(p => new
                            {
                                FM_ID = Convert.ToInt32(p["FM_ID"]),
                                FM_Name = p["FM_Name"].ToString(),
                                Investors = p["Investors"].ToString(),
                                Amount = p["Amount"].ToString(),
                                Status = "Error"
                            });
                            var errormsg = new {Status = "Error", Message = "Not allowed" };
                            var pet = new { Data = ErrStatus = contractList, AdditionalParameters = ErrMessage = errormsg};
                            return Request.CreateResponse(pet);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }
           
        }

       /* [Route("api/fundsummary/fileuploads")]
        [HttpPost]
        public string FileUploads()
        {
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    string userid = Request.Form["userid"];
                    string description = Request.Form["name"];
    
                    string fnam = "";

                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }
                        var supportedTypes = new[] { "csv" };
                        var fileExt = System.IO.Path.GetExtension(fname).Substring(1);
                        if (!supportedTypes.Contains(fileExt))
                        {
                            string ErrorMessage = "Error: File Extension Is InValid - Only CSV Files Allowed";
                            return ErrorMessage;
                        }


                        DateTime now = DateTime.Now;
                        fnam = now.ToFileTime() + fname;
                        fname = Path.Combine("C:/Uploads/Bluesky/", fnam);
                        file.SaveAs(fname);
                        // Get the complete folder path and store the file inside it.  
                        using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                        {
                            connection.Open();

                            using (var command = new SqlCommand("SP_FileImporter_CreateRecord", connection))
                            {
                                command.CommandType = CommandType.StoredProcedure;
                                command.Parameters.AddWithValue("@UserID", userid);
                               
                            }

                        }
                    }

                    //(string message, string status, int parameter, string filenm) p = (message: "File Uploaded Successfully", status: "success", parameter: lastrecord, filenm: fnam);

                    // Returns message that successfully uploaded  
                    return "File Uploaded Successfully";
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
            }
            else
            {
                return "Error: No files selected.";
            }
        }*/

        // POST api/<controller>
        public string Post(User usr)

        {

            try
            {
                string query = @"
                    insert into dbo.Users(U_FirstName, U_LastName, U_Title , U_Email, U_Phone, U_Username, U_Password) values
                    (
                    '" + usr.U_FirstName + @"'
                    ,'" + usr.U_LastName + @"'
                    ,'" + usr.U_Title + @"'
                    ,'" + usr.U_Email + @"'
                    ,'" + usr.U_Phone + @"'
                    ,'" + usr.U_Username + @"'
                    ,'" + usr.U_Password + @"'
                    )
                    ";

                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.
                    ConnectionStrings["BlueSkyConnectionString"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }

                return "User added Successfully!!";
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                return "Failed to Add!!";
            }
        }


        [Route("api/fundsummary/{iFM_ID}")]
        [HttpGet]
        public HttpResponseMessage FundInvestorInfo_Summary(int iFM_ID)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("FundInvestorInfo_Summary_Select", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@iFM_ID ", iFM_ID);
                        using (SqlDataReader response = command.ExecuteReader())
                        {
                            var responseList = response.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(x => new
                            {

                                FM_ID = x["FM_ID"].ToString(),
                                FM_Name = x["FM_Name"].ToString(),
                                FM_Currency = x["FM_Currency"].ToString(),
                                US_Investors = x["US_Investors"].ToString(),
                                Non_US_Investors = x["Non_US_Investors"].ToString(),
                                Investors = x["Investors"].ToString(),
                                US_Amount = x["US_Amount"].ToString(),
                                Non_US_Amount = x["Non_US_Amount"].ToString(),
                                Amount = x["Amount"].ToString(),
                                Status = "Success"
                            });

                            return Request.CreateResponse(responseList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }


        // PUT api/<controller>/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}