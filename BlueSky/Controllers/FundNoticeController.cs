﻿using BlueSky.Classes;
using BlueSky.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BlueSky.Controllers
{
    public class FundNoticeController : ApiController
    {
        public object ErrStatus { get; private set; }
        public object ErrMessage { get; private set; }
        // GET api/fundnotice
        public HttpResponseMessage Get()
        {
            try
            {
                return Request.CreateResponse("123");

            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }

        // POST api/<controller>

        public string Post(FundNoticeCompositeObject fnco)
        {

            //if (fm.Step == "1")
            //{
            try
            {

                DateTime d1 = DateTime.Parse(fnco.FundNotice.FN_FilingDate, null, System.Globalization.DateTimeStyles.RoundtripKind).ToLocalTime();

                string query = @"

                        exec [FundNotice_InsertUpdate]
                         '" + fnco.User.U_ID + @"'
                        , " + fnco.FundNotice.FN_ID + @"
                        , " + fnco.FundNotice.FN_FM_ID + @"
                        , " + fnco.FundNotice.FN_State + @"
                        , " + fnco.FundNotice.FN_Type_Filing + @"
                        , '" + d1.ToShortDateString() + @"'
                        , '" + fnco.FundNotice.FN_Exemption + @"'
                        , '" + fnco.FundNotice.FN_FilingDoc + @"'
                        , '" + fnco.FundNotice.FN_StateCorresDocID + @"'
                        , '" + fnco.FundNotice.FN_Notes + @"'
                        ";

                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["BlueSkyConnectionString"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }

                return "Completed Successfully!!";
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                return "Failed to Complete!!";
            }
            //}
            //    else
            //    {
            //        return "Nothing to Do";
            //    }
        }

        [Route("api/FundNotice/{iFM_ID}/{iInvestors}/{iLatest}")]
        [HttpGet]
        public HttpResponseMessage Get(int iFM_ID, int iInvestors, int iLatest)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("FundNotice_Select", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@iFM_ID", iFM_ID);
                        command.Parameters.AddWithValue("@iInvestors", iInvestors);
                        command.Parameters.AddWithValue("@iLatest", iLatest);
                        using (SqlDataReader contracts = command.ExecuteReader())
                        {
                            var dataList = contracts.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(p => new
                            {
                                FN_ID = Convert.ToInt32(p["FN_ID"]),
                                S_Name = p["S_Name"].ToString(),
                                FN_Number = p["FN_Number"].ToString(),
                                FT_Desc = p["FT_Desc"].ToString(),
                                FN_FilingDate = p["FN_FilingDate"].ToString(),
                                FN_Exemption = p["FN_Exemption"].ToString(),
                                FN_FilingDoc = p["FN_FilingDoc"].ToString(),
                                FN_StateCorresDocID = p["FN_StateCorresDocID"].ToString(),
                                FN_Notes = p["FN_Notes"].ToString(),
                                FN_State = p["FN_State"].ToString(),
                            });
                            return Request.CreateResponse(HttpStatusCode.OK, dataList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }
        }


        [Route("api/fndetails/{iFN_ID}")]
        [HttpGet]
        public HttpResponseMessage GetDetails(int iFN_ID)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    using (var command = new SqlCommand("FundNotice_Select1", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@FN_ID", iFN_ID);
                        using (SqlDataReader contracts = command.ExecuteReader())
                        {
                            var dataList = contracts.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(p => new
                            {

                                FN_ID = Convert.ToInt32(p["FN_ID"]),
                                FN_FM_ID = Convert.ToInt32(p["FN_FM_ID"]),
                                FN_State = Convert.ToInt32(p["FN_State"]),
                                FN_Number = Convert.ToInt32(p["FN_Number"]),
                                FN_Type_Filing = Convert.ToInt32(p["FN_Type_Filing"]),
                                FN_FilingDate = p["FN_FilingDate"].ToString(),
                                FN_Exemption = p["FN_Exemption"].ToString(),
                                FN_FilingDoc = p["FN_FilingDoc"].ToString(),
                                FN_StateCorresDocID = p["FN_StateCorresDocID"].ToString(),
                                FN_Notes = p["FN_Notes"].ToString(),

                            });
                            return Request.CreateResponse(dataList);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody] string value)
        {
        }

        [Route("api/fundnoticed")]
        [HttpPost]
        // DELETE api/<controller>/5
        public HttpResponseMessage Delete(FundNoticeHistoryDelete fnd)
        {
            try
            {
                string query = @"

                        exec [FundNotice_Delete]
                         " + fnd.FN_ID + @"
                        ";

                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["BlueSkyConnectionString"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }

                var pet = new { Status = "Success", Message = "Record Deleted" };
                return Request.CreateResponse(HttpStatusCode.OK, pet);
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }
        }
    }
}