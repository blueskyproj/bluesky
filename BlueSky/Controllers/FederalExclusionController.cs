﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BlueSky.Classes;
using BlueSky.Models;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;

namespace BlueSky.Controllers
{
    public class FederalExclusionController : ApiController
    {

        public object ErrStatus { get; private set; }
        public object ErrMessage { get; private set; }
        // GET api/federalexclusion
        public HttpResponseMessage Get()
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStrings.DBConnectionString))
                {
                    connection.Open();

                    DataTable table = new DataTable();

                    using (var command = new SqlCommand("FederalExclusion_Select", connection))
                    {

                        command.CommandType = CommandType.StoredProcedure;
                        using (SqlDataReader federalExclusion = command.ExecuteReader())
                        {
                            var dataList = federalExclusion.ConvertToDataTable().AsEnumerable().ToList().ConvertAll(p => new
                            {
                                FE_ID = Convert.ToInt32(p["FE_ID"]),
                                FE_Description = p["FE_Description"].ToString()
                            });

                            return Request.CreateResponse(HttpStatusCode.OK, dataList);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Common.ErrorLog(System.Reflection.MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                var pet = new { Status = "Error", Message = ex };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pet);
            }

        }
    }
}