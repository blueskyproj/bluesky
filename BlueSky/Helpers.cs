﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;

namespace BlueSky
{
    public static class Helpers
    {
        public static bool isValidEmailAddress(this string emailAddress)
        {
            return new Regex(@"^[^<>\s\@]+(\@[^<>\s\@]+(\.[^<>\s\@]+)+)$").IsMatch(emailAddress);
        }

        public static string IntArrayToCommaString(this int[] source)
        {
            return string.Join(",", source.ToArray());
        }
        public static string IntArrayToCommaStringTwo(int[] intArray)
        {
            string s = "";
            foreach (int i in intArray)
            {
                s += intArray[i].ToString() + ",";
            }
            return s.TrimEnd(',');
        }

        public static int[] CommaStringToIntArray(this string source)
        {
            return source.Split(',').Select(int.Parse).ToArray();
        }

        public static string[] CommaStringToArray(this string source)
        {
            return source.Split(',').ToArray();
        }

        public static string RemoveXmlNameSpace(this string xml)
        {
            return Regex.Replace(xml, @" xmlns[^""]+""[^""]+[""]+", string.Empty);
        }

        public static DataTable ConvertToDataTable(this SqlDataReader SQLDataReader)
        {
            var dataTable = new DataTable();

            dataTable.Load(SQLDataReader);

            return dataTable;
        }

        public static int AsInteger(this string s)
        {
            int i;

            if (Int32.TryParse(s, out i)) return i;

            return 0;
        }


        public static double AsDouble(this string s)
        {
            double value;

            if (Double.TryParse(s, out value)) return value;

            return 0;
        }

        public static float AsFloat(this string s)
        {
            float value;

            if (float.TryParse(s, out value)) return value;

            return 0;
        }

        public static DataTable ConvertToDataTable<T>(this IEnumerable<T> collection)
        {
            DataTable dt = new DataTable("DataTable");
            Type t = typeof(T);
            PropertyInfo[] pia = t.GetProperties();

            //Inspect the properties and create the columns in the DataTable
            foreach (PropertyInfo pi in pia)
            {
                Type ColumnType = pi.PropertyType;
                if ((ColumnType.IsGenericType))
                {
                    ColumnType = ColumnType.GetGenericArguments()[0];
                }
                dt.Columns.Add(pi.Name, ColumnType);
            }

            //Populate the data table
            foreach (T item in collection)
            {
                DataRow dr = dt.NewRow();
                dr.BeginEdit();
                foreach (PropertyInfo pi in pia)
                {
                    if (pi.GetValue(item, null) != null)
                    {
                        dr[pi.Name] = pi.GetValue(item, null);
                    }
                }
                dr.EndEdit();
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public static string RemoveSpecialCharacters(this string _value)
        {
            Regex regex = new Regex("(?:[^a-zA-Z0-9 ]|(?<=['\"])s)");

            return regex.Replace(_value, string.Empty);

        }

        public static bool ContainsSpecialCharacters(this string _value)
        {
            Regex regex = new Regex("(?:[^a-zA-Z0-9 ]|(?<=['\"])s)");

            return regex.IsMatch(_value);

        }

        public static string RemoveLeadingCharacters(this string _value, string strToRemove)
        {
            if (_value.StartsWith(strToRemove) && _value.Length > _value.IndexOf(strToRemove) + 1)
            {
                return _value.Substring(_value.IndexOf(strToRemove) + 1);
            }
            else
            {
                return _value;
            }



        }

       
        public static string URIPageName(this Uri uri)
        {
            Uri url = new Uri(uri.ToString());

            string strUrl = "";

            int Pos = url.ToString().IndexOf("?");
            if (Pos < 0)
            {
                strUrl = url.PathAndQuery;
            }
            else
            {
                strUrl = url.PathAndQuery.Substring(0, url.PathAndQuery.IndexOf("?"));
            }

            strUrl = strUrl.Substring(1); //remove leading "/"

            if (strUrl == "") { strUrl = "home/index"; }

            return strUrl;
        }

        public static DateTime GetCorrectDateFormat(object date)
        {
            var emptyDate = "0001/01/01 12:00:00 AM";
            if (date.ToString() == emptyDate.ToString()) return DateTime.Parse(emptyDate); //DateTime.MinValue; //
            return (DateTime)date;
        }   
    }
}