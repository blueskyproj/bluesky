﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlueSky.Models
{
    public class FundNotice
    {

        public int FN_ID { get; set; }
        public int FN_FM_ID { get; set; }
        public int FN_State { get; set; }
        public int FN_Number { get; set; }
        public int FN_Type_Filing { get; set; }
        public string FN_FilingDate { get; set; }
        public string FN_Exemption { get; set; }
        public string FN_FilingDoc { get; set; }
        public string FN_StateCorresDocID { get; set; }
        public string FN_Notes { get; set; }


    }

    public class FundNoticeHistoryPost
    {

        public int FN_FM_ID { get; set; }
        public int FN_State { get; set; }


    }

    public class FundNoticeHistoryDelete
    {
        public int FN_ID { get; set; }


    }

    public class FundNoticeCompositeObject
    {
        public FundNotice FundNotice { get; set; }
        public User User { get; set; }
    }

}