﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlueSky.Models
{
    public class FundInvestorInfo
    {

        public int FI_ID { get; set; }
        public int FI_FM_ID { get; set; }
        public string FI_Name { get; set; }
        public int FI_Country { get; set; }
        public int FI_Jur { get; set; }
        public float FI_SubsAmnt { get; set; }
        public DateTime FI_CommitDate { get; set; }
        public int FI_New { get; set; }


    }

    public class FundInvestorInfoCompositeObject
    {
        public FundInvestorInfo FundInvestorInfo { get; set; }
        public User User { get; set; }
    }
}