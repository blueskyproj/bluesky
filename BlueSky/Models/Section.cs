﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlueSky.Models
{
    public class Section
    {
        public int TS_ID { get; set; }
        public string TS_Section { get; set; }
    }
}