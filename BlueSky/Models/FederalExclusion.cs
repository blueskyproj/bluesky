﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlueSky.Models
{
    public class FederalExclusion
    {
        public int FE_ID { get; set; }
        public string FE_Description { get; set; }
    }
}