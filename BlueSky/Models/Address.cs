﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlueSky.Models
{
    public class Address
    {
        public int Adr_ID { get; set; }
        public int Adr_Type { get; set; }
        public int Adr_Link_ID { get; set; }
        public string Adr_Line1 { get; set; }
        public string Adr_Line2 { get; set; }
        public string Adr_City { get; set; }
        public string Adr_State { get; set; }
        public string Adr_Zip { get; set; }
        public int Adr_Country { get; set; }
        public string Adr_Phone { get; set; }
        public int Adr_Default { get; set; }
        public string Step { get; set; }
        public int UserID { get; set; }
    }
}