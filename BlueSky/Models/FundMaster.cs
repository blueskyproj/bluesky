﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlueSky.Models
{
    public class FundMaster
    {

        public int FM_ID { get; set; }
        public int FM_M_ID { get; set; }
        public string FM_Name { get; set; }
        public string FM_PreviousName { get; set; }
        public int FM_Industry_Group { get; set; }
        public int FM_Federal_Exemption { get; set; }
        public string FM_Phone { get; set; }
        public string FM_MatterCode { get; set; }
        public string FM_Client { get; set; }
        public string FM_BillingPartner { get; set; }
        public string FM_PartnerResp { get; set; }
        public string FM_Team_Members { get; set; }
        public string FM_CIK { get; set; }
        public string FM_CCC { get; set; }
        public string FM_Passphrase { get; set; }
        public string FM_FormDFiling { get; set; }
        public string FM_JurisOrg { get; set; }
        public int FM_DateOrg { get; set; }
        public int FM_EntityType { get; set; }
        public string FM_DateFirstSale { get; set; }
        public string FM_Currency { get; set; }
        public int FM_1940ActExclusion { get; set; }
        public string FM_MinInvest { get; set; }
        public string Step { get; set; }
        public int UserID { get; set; }
    }
}