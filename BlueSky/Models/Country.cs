﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlueSky.Models
{
    public class Country
    {

        public int C_ID { get; set; }
        public string C_Code { get; set; }
        public string C_Name { get; set; }
        public string C_CodeName { get; set; }
        public int C_Category { get; set; }
        public int C_Order { get; set; }
        public int C_Inactive { get; set; }
        
    }
}