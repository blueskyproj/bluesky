﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlueSky.Models
{
    public class User
    {
        public int U_ID { get; set; }
        public string U_Username { get; set; }
        public string U_FirstName { get; set; }
        public string U_LastName { get; set; }
        public string U_Title { get; set; }
        public string U_Gender { get; set; }
        public string U_Email { get; set; }
        public string U_Phone { get; set; }
        public string U_Password { get; set; }
        public string U_Picture { get; set; }
        public int U_Status { get; set; }
        public DateTime U_DateCreated { get; set; }
        public DateTime U_DateUpdate { get; set; }
    }
}