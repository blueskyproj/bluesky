﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlueSky.Models
{
    public class IndustryGroup
    {
        public int IG_ID { get; set; }
        public string IG_Group { get; set; }
    }
}