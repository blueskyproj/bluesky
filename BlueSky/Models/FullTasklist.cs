﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlueSky.Models
{
    public class FullTasklist
    {
        public string id { get; set; }
        public int section { get; set; }
        public string title { get; set; }
        public string notes { get; set; }
        public int priority { get; set; }
        public int fund { get; set; }
        public string dueDate { get; set; }
        public string doneDate { get; set; }
        public string type { get; set; }
        public int DoneBy { get; set; }
        public int allocated { get; set; }
        public bool completed { get; set; }
        public string[] tags { get; set; }
        public int order { get; set; }

    }

    //public class RootObject
    //{
    //    public List<FullTasklist> items { get; set; }
    //}
}