﻿using System.Configuration;

namespace BlueSky.Classes
{
    public static class ConnectionStrings
    {
        public static string DBConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["BlueSkyConnectionString"].ToString(); }
        }
    }
}