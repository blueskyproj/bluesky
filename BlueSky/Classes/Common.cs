﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BlueSky.Classes
{
    public class Common
    {
            internal static void ErrorLog(string errormsg, string section)
        {            
            using (SqlConnection connection = new SqlConnection(ConnectionStrings.DBConnectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand("[sp_ErrorLog]", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@Error", SqlDbType.VarChar, 500).Value = errormsg;
                    command.Parameters.Add("@Section", SqlDbType.VarChar, 250).Value = section;
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}

